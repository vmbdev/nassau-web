const express = require('express');
const app = express();
const port = process.env.PORT || 8080;
const hbs = require('hbs');
const bodyParser = require('body-parser');
const livroRoutes = require('./routes/livro');
const mongoose = require('mongoose');
const flash = require('connect-flash');
const session = require('express-session');
const methodOverride = require('method-override');

mongoose.connect('mongodb://nassauweb_user:nassauweb123@ds159507.mlab.com:59507/nassauweb');
mongoose.connection
 .once('open', () => {
  console.log('Tudo Tranquilo! Tudo Favoravel!!!!');
})
.on('error', (error) => {
  console.warn(`Deu bad parcero: ${error}`);
});
app.use(methodOverride('_method'));

mongoose.Promise = global.Promise;


app.use( express.static(__dirname + '/public'));

app.set('view engine','hbs');

hbs.registerPartials(__dirname + '/views/partials');

app.use(bodyParser.urlencoded({ extended: true }));

app.use(flash());
app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true,
  cookie: { maxAge: 60000 }
}));

app.use(function(req, res, next){
  res.locals.success = req.flash('success');
  res.locals.error =  req.flash('error');
  res.locals.info = req.flash('info');
  
  next();
});

app.use('/livros', livroRoutes);

app.get('/', (req, res) => {
 // res.send('Epaaaa!');
  res.render('index');
});
// app.get('/', (req, res) => {
//   res.send('livrosss');
// });

app.listen(port, () =>{
  console.log(`The server is running on port ${port}.`);
});